<?php
namespace App\Services;

use App\Models\Project;
use App\Repositories\ProjectRepository;

class ProjectServices
{
    /**
     * @var project
     */
    protected ProjectRepository $projectRepository;


    /**
     * ProjectRepository constructor.
     *
     * @parm Project $projcet
     */
    public function __construct(ProjectRepository $projectRepository) {
        $this->projectRepository = $projectRepository;
    }

    /**
     * getAllProjects function
     *
     * @return array
     */
    public function getAllProjects() {
        return $this->projectRepository->getAllProjects();
    }


    /**
     * getAllProjectsAjax function
     *
     * @return array
     */
    public function getAllProjectsAjax() {
        return $this->projectRepository->getAllProjectsAjax();
    }

    /**
     * getLastProjects function
     *
     * @param $lastN
     * @return array
     */
    public function getLastProjects($lastN) {
        return $this->projectRepository->getLastProjects($lastN);
    }

    /**
     * createProject function
     *
     * @param $requset
     * @return array
     */
    public function createProject($requset) {
        return $this->projectRepository->create($requset);
    }

    /**
     * updateProject function
     *
     * @param $requset , $projectId
     * @return array
     */
    public function updateProject($projectId, $requset) {
        $project = $this->projectRepository->findById($projectId);
        if($project) {
            return $this->projectRepository->update($projectId, $requset);
        }
    }

    /**
     * getProject function
     *
     * @param $projectId
     * @return array
     */
    public function getProject($projectId) {
        return $this->projectRepository->findById($projectId);
    }

    /**
     * deleteProject function
     *
     * @param $projectId
     * @return array
     */
    public function deleteProject($projectId) {
        $project = $this->projectRepository->findById($projectId);
        return $this->projectRepository->delete($project);
    }

}
