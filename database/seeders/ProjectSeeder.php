<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 1;
        while($count < 5) {
            DB::table('projects')->insert([
                'title' => 'Project '.$count.' Name',
                'href' => 'https://www.google.com',
                'image' => 'http://127.0.0.1:8000/front/img/projectImg/'.$count.'.png',
                'meta_tags' => Str::random(5).', '.Str::random(10)
            ]);
            $count++;
        }
    }
}
