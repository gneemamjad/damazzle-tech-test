@extends('layouts.front')

@section('content')

    <nav class="navbar navbar-expand-lg navbar">
        <div class="container-fluid">
            <div class="col-sm-4">
                <ul class="nav justify-content navleft">
                    <li class="nav-item">
                        <a class="nav-link" href="#ourWorkSection">Work</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#aboutSection">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ourTeamSection">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">Blog</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 text-center">
                <a href="/">
                    <span class="brand-logo">
                            <svg width="232" height="61" viewBox="0 0 232 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M35.0726 28.895L24.1394 1.51758H23.3328L12.3101 28.895H35.0726ZM23.6913 8.11669L30.771 26.2196H16.6116L23.6913 8.11669Z" fill="#333333"/>
                                <path d="M44.1236 59.3047H47.2602L36.327 32.4624H11.1449L0.12207 59.3047H3.34825L13.0268 35.1377H34.3554L44.1236 59.3047Z" fill="#333333"/>
                                <path d="M59.8063 30.4992C59.8063 29.9642 59.8063 29.5183 59.8063 28.894H56.5801C56.5801 29.5183 56.5801 29.9642 56.5801 30.4992C56.5801 31.0343 56.5801 31.4802 56.5801 32.4611H59.8063C59.8063 31.4802 59.8063 31.0343 59.8063 30.4992Z" fill="#333333"/>
                                <path d="M87.4082 3.21158C93.5917 3.21158 99.3271 5.26265 103.987 8.82974L105.869 6.68949C100.761 2.58734 94.3982 0.35791 87.4082 0.35791C71.2773 0.35791 59.2687 11.4159 57.2075 26.0409H60.1649C62.226 13.1102 72.98 3.21158 87.4082 3.21158Z" fill="#333333"/>
                                <path d="M87.4079 57.8774C72.9797 57.8774 62.2258 48.7813 60.1646 35.1372H57.1177C59.1788 50.4757 71.277 60.6419 87.3183 60.6419C94.3084 60.6419 100.761 58.4124 105.779 54.3103L103.897 52.17C99.2373 55.7371 93.5018 57.8774 87.4079 57.8774Z" fill="#333333"/>
                                <path d="M148.168 23.901L120.208 1.51758V5.44137L148.168 27.7357L175.77 5.44137V1.51758L148.168 23.901Z" fill="#333333"/>
                                <path d="M148.168 31.6589L120.208 9.27539V58.3228H123.613V15.6961L148.168 35.5826L173.081 15.6961V58.3228H175.59V9.27539L148.168 31.6589Z" fill="#333333"/>
                                <path d="M197.278 29.7866H225.955V27.1113H197.278V6.60059H194.589V58.3233H231.332V55.648H197.278V29.7866Z" fill="#333333"/>
                                <path d="M231.332 27.1113H228.644V29.7866H231.332V27.1113Z" fill="#333333"/>
                                <path d="M231.332 1.25H194.589V3.92531H231.332V1.25Z" fill="#333333"/>
                            </svg>
                    </span>
                </a>
            </div>
            <div class="col-sm-4">
                <ul class="nav justify-content-end navright">
                    <li class="nav-item">
                        <a class="nav-link" href="tel:+99999999999">+999 9999 9999</a>
                    </li>
                    <li class="nav-item">
                        <a href="tel:+99999999999" class="nav-link btn btn-contact">Contact us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" id="aboutSection">
        <div class="row aboutRow">
            <div data-aos="fade-right" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                <h1 class="headingText">Our<span class="behindCircle"><svg width="65" height="65" viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="32.6338" cy="32.2163" r="31.8974" fill="#7C4DFF"/></svg></span><br>Service <span class="lastCircle"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="12.3995" cy="12.1754" r="11.5562" fill="#7C4DFF"/></svg></span></h1>
                <h3 class="headingDescreption"><strong>Thing</strong> <i>Outside the Box</i> <strong>, Design, Develope & Lead.</strong></h3>
            </div>
            <div data-aos="fade-left" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                <div class="row">
                    <div class="col-6">
                        <h3 class="subHeading">
                            <span>
                                <svg width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8.24573" cy="7.58899" r="7.51868" fill="#FECF08"/>
                                    <circle cx="15.7643" cy="7.58901" r="7.0187" stroke="#FECF08"/>
                                </svg>
                            </span>
                            Media Art Production
                        </h3>
                    </div>
                    <div class="col-6">
                        <h3 class="subHeading">
                            <span>
                                <svg width="25" height="16" viewBox="0 0 25 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="16.6012" cy="8.10757" r="7.0187" stroke="#9B51E0"/>
                                    <circle cx="8.2594" cy="8.10754" r="7.51868" fill="#BE08FE"/>
                                </svg>
                            </span>
                            Digital Marketing
                        </h3>
                        <p class="subHeadingDescreption">Markiting And Media<br>(On/Off) Line</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 aboutParagraph">
                        <p>We combine Strategy and production in three main keys</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="welcomeSection">
        <div data-aos="fade-left" class="row">
            <div class="col-12 text-center">
                <h1 class="headingText"><strong>WELCOME TO</strong><br>OUR KITCHEN</h1>
            </div>
        </div>
        <div data-os="zoom-in" class="row welcomeImg">
            <div class="col-12">
                <img src="{{asset('front/img/welcomeBG.png')}}" alt="">
            </div>
        </div>
        <div data-os="fade-right" clas="row">
            <div class="col-12 text-center subHeading">
                <h4>At ACME , We are a little family. working together as<br>
                    like as family restaurant.<br>
                    work hard to service client with the best experience that can have. </h4>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="secondWelcomeSection">
        <div class="row">
            <div class="col-12 col-sm-6 imageDev">
                <div class="img">
                    <img src="{{asset('front/img/2ndAboutImg-2.png')}}" alt="">
                </div>
                <div class="img">
                    <img src="{{asset('front/img/2ndAboutImg-1.png')}}" alt="">
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blandit diam, elementum imperdiet amet amet felis. Malesuada aliquam tristique faucibus massa quam massa purus. Morbi fusce tortor gravida <br>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blandit diam, elementum imperdiet amet amet felis. Malesuada aliquam tristique faucibus massa quam massa purus. Morbi fusce tortor gravida <br>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blandit diam, elementum imperdiet amet amet felis. Malesuada aliquam tristique faucibus massa quam massa purus. Morbi fusce tortor gravida
                </p>
            </div>
        </div>
    </div>

    <div data-aos="fade-right" class="container-fluid" id="ourTeamSection">
        <div class="row">
            <div class="col-12 heading text-center">
                <h1 class="headingText">
                    Our Secrite recipe
                </h1>
                <p class="subHeading">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blandit diam, elementum<br>
                    imperdiet amet amet felis. Malesuada aliquam tristique faucibus massa quam<br>
                    massa purus. Morbi fusce tortor gravida </p>
            </div>
        </div>
        <div class="row ourTeamImg">
            <div class="col-12">
                <img src="{{asset('front/img/ourTeam.png')}}" alt="">
            </div>
        </div>
        <div class="row ourTeamService text-center">
            <div class="col-12">
                <h2 class="headingText">
                    We combine strategy and production<br>
                    in three main keys
                </h2>
            </div>
            <div class="col-sm-4 col-12">
                <span>
                    <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="13.9841" cy="13.4841" r="13.2585" fill="#FECF08"/>
                        <circle cx="27.2419" cy="13.4841" r="12.3768" stroke="#FECF08" stroke-width="1.76341"/>
                    </svg>
                </span>
                <h3>Media</h3>
                <p>
                    Media Art<br>
                    Production
                </p>
            </div>
            <div class="col-sm-4 col-12">
                <span>
                    <svg width="39" height="27" viewBox="0 0 39 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="25.3406" cy="13.4841" r="12.3768" stroke="#9B51E0" stroke-width="1.76341"/>
                        <circle cx="14.1579" cy="13.4841" r="13.2585" fill="#BE08FE"/>
                    </svg>
                </span>
                <h3>Management</h3>
                <p>
                    MARKETING AND MEDIA<br>
                    ONLINE/OFFLINE
                </p>
            </div>
            <div class="col-sm-4 col-12">
                <span>
                    <svg width="41" height="27" viewBox="0 0 41 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="27.4655" cy="13.4841" r="12.3768" stroke="#00438C" stroke-width="1.76341"/>
                        <circle cx="14.2077" cy="13.4841" r="13.2585" fill="#00438C"/>
                    </svg>
                </span>
                <h3>Tech</h3>
                <p>
                    Build Smart<br>
                    Creative Solution
                </p>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="ourWorkSection">
        <div class="row">
            <div class="col-12">
                <h1 class="headingText">Our Media Work</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ourWorkCol">
                <div class="slider">
                    <div class="slider-inner">
                        @foreach($projects as $project)
                            <div class="slide projectItem">
                                <div class="image">
                                    @if($project->href)
                                        <span class="dot" onclick="goTo('{{$project->href}}')"><a target="_blank" href="{{$project->href}}">Discover</a></span>
                                    @endif
                                    <img src="{{$project->image}}" alt="">
                                </div>
                                <div class="projectDescription">
                                    <h2>{{$project->title}}</h2>
                                    <p>{{$project->meta_tags}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="row shortHeading">
            <div class="col-12 col-sm-4 col-md-12 col-lg-4">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 ">
                        <svg width="94" height="49" viewBox="0 0 94 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M93.84 48.1919H0V0.921875H93.84V48.1919ZM1.62268 46.5707H92.2173V2.54308H1.62268V46.5707Z" fill="white"/>
                            <g opacity="0.33">
                                <path d="M21.4214 12.233C22.2789 12.233 22.9763 17.3627 22.9763 23.7063C22.9763 30.0396 22.2789 35.1796 21.4214 35.1796C21.2923 35.1796 21.1683 35.0711 21.0495 34.8593C21.0495 34.8593 21.0495 34.8593 21.0495 34.8541C20.3676 33.6298 19.8613 29.1046 19.8613 23.7115C19.8613 18.308 20.3727 13.7828 21.0495 12.5688C21.0495 12.5637 21.0495 12.5637 21.0495 12.5637C21.1734 12.3415 21.2922 12.233 21.4214 12.233Z" fill="white"/>
                                <path d="M23.0366 23.6984C23.0366 29.3601 22.4374 35.2233 21.4301 35.2233C21.2803 35.2233 21.146 35.1096 21.0117 34.8772L21.0065 34.8565C20.3039 33.5702 19.8184 28.9933 19.8184 23.6984C19.8184 18.3982 20.3091 13.8162 21.0065 12.5402L21.0117 12.5195C21.146 12.2871 21.2854 12.1734 21.4301 12.1734C22.4323 12.1734 23.0366 18.0366 23.0366 23.6984ZM21.1046 34.8358C21.2131 35.0218 21.3216 35.1148 21.4249 35.1148C22.1533 35.1148 22.9282 30.5275 22.9282 23.6984C22.9282 16.8692 22.1533 12.2819 21.4249 12.2819C21.3216 12.2819 21.2131 12.3749 21.1046 12.566L21.0995 12.5816C20.4021 13.8265 19.9165 18.3982 19.9165 23.6984C19.9165 28.9933 20.4021 33.5651 21.0995 34.8152L21.1046 34.8358Z" fill="white"/>
                                <path d="M73.3872 1.92798L21.1982 13.5534V34.4006L73.3872 47.0266H92.4547V1.92798H73.3872Z" fill="white"/>
                            </g>
                            <g opacity="0.66">
                                <path d="M21.4214 15.6028C22.2789 15.6028 22.9763 19.3207 22.9763 23.9185C22.9763 28.5087 22.2789 32.2341 21.4214 32.2341C21.2923 32.2341 21.1683 32.1555 21.0495 32.002C21.0495 32.002 21.0495 32.002 21.0495 31.9983C20.3676 31.1109 19.8613 27.8311 19.8613 23.9222C19.8613 20.0059 20.3727 16.726 21.0495 15.8462C21.0495 15.8424 21.0495 15.8424 21.0495 15.8424C21.1734 15.6814 21.2923 15.6028 21.4214 15.6028Z" fill="white"/>
                                <path d="M23.0366 23.9128C23.0366 28.0163 22.4374 32.2659 21.4301 32.2659C21.2803 32.2659 21.146 32.1835 21.0117 32.015L21.0065 32C20.3039 31.0678 19.8184 27.7505 19.8184 23.9128C19.8184 20.0713 20.3091 16.7503 21.0065 15.8255L21.0117 15.8105C21.146 15.642 21.2854 15.5596 21.4301 15.5596C22.4323 15.5596 23.0366 19.8092 23.0366 23.9128ZM21.1046 31.9851C21.2131 32.1198 21.3216 32.1872 21.4249 32.1872C22.1533 32.1872 22.9282 28.8625 22.9282 23.9128C22.9282 18.963 22.1533 15.6383 21.4249 15.6383C21.3216 15.6383 21.2131 15.7057 21.1046 15.8442L21.0995 15.8554C20.4021 16.7578 19.9165 20.0713 19.9165 23.9128C19.9165 27.7505 20.4021 31.064 21.0995 31.9701L21.1046 31.9851Z" fill="white"/>
                                <path d="M92.4547 45.9215L92.4547 2.8905L21.1982 16.5598L21.1982 31.6695L92.4547 45.9215Z" fill="white"/>
                            </g>
                            <path d="M21.4214 19.401C22.2789 19.401 22.9763 21.5148 22.9763 24.1288C22.9763 26.7386 22.2789 28.8567 21.4214 28.8567C21.2923 28.8567 21.1683 28.812 21.0495 28.7247C21.0495 28.7247 21.0495 28.7247 21.0495 28.7226C20.3676 28.2181 19.8613 26.3533 19.8613 24.131C19.8613 21.9043 20.3727 20.0396 21.0495 19.5393C21.0495 19.5372 21.0495 19.5372 21.0495 19.5372C21.1734 19.4457 21.2923 19.401 21.4214 19.401Z" fill="white"/>
                            <path d="M23.0366 24.1256C23.0366 26.4587 22.4374 28.8748 21.4301 28.8748C21.2803 28.8748 21.146 28.8279 21.0117 28.7321L21.0065 28.7236C20.3039 28.1936 19.8184 26.3075 19.8184 24.1256C19.8184 21.9416 20.3091 20.0534 21.0065 19.5276L21.0117 19.5191C21.146 19.4233 21.2854 19.3765 21.4301 19.3765C22.4323 19.3765 23.0366 21.7926 23.0366 24.1256ZM21.1046 28.7151C21.2131 28.7917 21.3216 28.8301 21.4249 28.8301C22.1533 28.8301 22.9282 26.9398 22.9282 24.1256C22.9282 21.3115 22.1533 19.4212 21.4249 19.4212C21.3216 19.4212 21.2131 19.4595 21.1046 19.5383L21.0995 19.5446C20.4021 20.0577 19.9165 21.9416 19.9165 24.1256C19.9165 26.3075 20.4021 28.1914 21.0995 28.7066L21.1046 28.7151Z" fill="white"/>
                            <path d="M92.4547 36.6386L92.4547 12.1735L21.1982 19.9451L21.1982 28.5357L92.4547 36.6386Z" fill="white"/>
                            <path d="M21.5651 13.0769C22.2418 13.0769 22.7894 17.8295 22.7894 23.6978C22.7894 29.561 22.2419 34.3187 21.5651 34.3187C20.8884 34.3187 20.3408 29.5662 20.3408 23.6978C20.3408 17.8346 20.8884 13.0769 21.5651 13.0769Z" fill="white"/>
                            <path d="M18.5634 30.248C16.7812 30.248 15.2573 29.6127 13.9866 28.3419C12.7158 27.0711 12.0752 25.542 12.0752 23.7649C12.0752 21.9001 12.7106 20.3297 13.9865 19.0589C15.2573 17.7881 16.7812 17.1527 18.5634 17.1527C20.4283 17.1527 21.9987 17.7881 23.2695 19.0589C24.5403 20.3297 25.1757 21.9001 25.1757 23.7649C25.1757 25.5472 24.5403 27.0711 23.2695 28.3419C21.9987 29.6127 20.4283 30.248 18.5634 30.248Z" fill="white"/>
                            <path d="M20.7542 12.556C20.8679 12.5508 20.966 12.5508 21.0538 12.556C21.2966 12.556 21.4258 12.5611 21.4258 12.5611L21.4309 13.1345L21.5601 23.7038L21.4309 34.273L21.4258 34.8413C21.4258 34.8413 21.3793 34.8413 21.276 34.8464L21.0538 34.8464C20.9815 34.8464 20.8885 34.8516 20.7955 34.8516C18.5484 34.8671 10.872 34.7431 6.77037 32.372C3.4126 30.4348 2.2193 27.8881 1.81636 26.0542C1.63039 25.238 1.6149 24.5665 1.62523 24.1532C1.62523 23.8691 1.65105 23.7038 1.65105 23.7038C1.65105 23.7038 1.62523 23.5385 1.62523 23.2544C1.60973 22.8411 1.63039 22.1695 1.81636 21.3533C2.2193 19.5195 3.4126 16.9676 6.77037 15.0356C10.8565 12.6696 18.4606 12.5353 20.7542 12.556Z" fill="white"/>
                            <path d="M21.6533 23.6982L21.5138 34.9235L21.426 34.9235C21.426 34.9235 21.3795 34.9235 21.2813 34.9286L21.0489 34.9286C21.0127 34.9286 20.9765 34.9286 20.9404 34.9338C20.8939 34.9338 20.8422 34.939 20.7906 34.939C18.528 34.9545 10.8154 34.815 6.71892 32.449C3.30948 30.4809 2.12136 27.898 1.72359 26.0744C1.53762 25.2531 1.51696 24.5764 1.53246 24.1476C1.53246 23.9048 1.55311 23.7498 1.55828 23.7033C1.55311 23.6568 1.53246 23.5019 1.53246 23.2539C1.51696 22.8303 1.54279 22.1536 1.72359 21.3322C2.12136 19.5087 3.31465 16.9206 6.71892 14.9576C10.8516 12.571 18.497 12.4522 20.7441 12.4729C20.8629 12.4677 20.961 12.4677 21.054 12.4729C21.2968 12.4729 21.426 12.478 21.426 12.478L21.5086 12.4832L21.6533 23.6982ZM21.3433 34.753L21.4776 23.6982L21.3433 12.6433C21.2813 12.6433 21.1832 12.6382 21.054 12.6382C20.961 12.633 20.8732 12.633 20.7596 12.6382L20.7544 12.6382C18.5176 12.6175 10.9084 12.7363 6.8119 15.1022C3.45929 17.0343 2.29183 19.5758 1.89923 21.3632C1.71842 22.1639 1.69775 22.8251 1.71325 23.2436C1.71325 23.5174 1.73909 23.6775 1.73909 23.6827L1.73909 23.6982L1.73909 23.7137C1.73909 23.7137 1.71325 23.8738 1.71325 24.1476C1.69775 24.566 1.72359 25.2272 1.89923 26.0331C2.29183 27.8256 3.45929 30.3621 6.8119 32.2941C10.8722 34.6393 18.5383 34.7788 20.7906 34.7633C20.8422 34.7633 20.8887 34.7633 20.9301 34.7582C20.9714 34.7582 21.0127 34.753 21.0489 34.753L21.271 34.753C21.302 34.753 21.3227 34.753 21.3433 34.753Z" fill="white"/>
                        </svg>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9 officeStyle">
                        <h2>Office</h2>
                        <a href="">Location on mapi</a>
                        <br><br>
                        <p>
                            Available Everyday<br>
                            <span>From 9:00 AM to 6:00 PM</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-2 col-md-3 col-lg-2 servicesStyle">
                <h2>Services</h2>
                <ul>
                    <li>Digital Management</li>
                    <li>Media Art Production</li>
                </ul>
            </div>
            <div class="col-6 col-sm-2 col-md-3 col-lg-2 quickLinkStyle">
                <h2>Quick links</h2>
                <ul>
                    <li><a href="">What’s ACME ?</a></li>
                    <li><a href="">About us</a></li>
                    <li><a href="">Our Services</a></li>
                    <li><a href="">Our Work</a></li>
                    <li><a href="">Our Blog</a></li>
                    <li><a href="">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-6 col-sm-2 col-md-3 col-lg-2 contactStyle">
                <h2>Careers</h2>
                <a href="mailto:hr@info.com">hr@info.com</a>

                <h2 class="emailUs">Email US</h2>
                <a href="mailto:info@info.com">info@info.com</a>
            </div>
            <div class="col-6 col-sm-2 col-md-3 col-lg-2 callUsStyle">
                <h2>Call US</h2>
                <ul>
                    <li><a href="tel:+99999999999">+999 9999 9999</a></li>
                    <li><a href="tel:+99999999999">+999 9999 9999</a></li>
                </ul>
                <ul class="socialMedia">
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/Facebook.svg')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/Twitter.svg')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/LinkedIn.svg')}}" alt=""></a></li>
                    <br>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/YouTube.svg')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/Behance.svg')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/Instagram.svg')}}" alt=""></a></li>
                    <br>
                    <li><a href=""><img src="{{asset('front/img/socialMediaLogo/Vimeo.svg')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <nav class="nav">
                    <a class="nav-link" href="#">Privacy policy</a>
                    <a class="nav-link" href="#">Term and conditions</a>
                    <a class="nav-link" href="#">About us</a>
                </nav>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 copyRightText">
                <p>Copyright © 2022 ACME  Tech.<br>All Rights Reserved</p>
            </div>
        </div>
    </footer>

@endsection
