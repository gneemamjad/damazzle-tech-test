let draggableSlider = function () {
    // DOM element(s)
    let slider = document.querySelector(".slider"),
        innerSlider = document.querySelector(".slider-inner");

    // Slider variables
    let pressed = false,
        startX,
        x;

    // Mousedown eventlistener
    slider.addEventListener("mousedown", (e) => {
        pressed = true;
        startX = e.offsetX - innerSlider.offsetLeft;
        slider.style.cursor = "grabbing";
    });

    // mouseneter
    slider.addEventListener("mouseenter", () => {
        slider.style.cursor = "grab";
    });

    // mouseup
    slider.addEventListener("mouseup", () => {
        slider.style.cursor = "grab";
    });

    // window
    window.addEventListener("mouseup", () => {
        pressed = false;
    });

    // Slider mousemove event listener
    slider.addEventListener("mousemove", (e) => {
        if (!pressed) return;
        e.preventDefault();

        x = e.offsetX;

        innerSlider.style.left = `${x - startX}px`;

        checkBoundry();
    });

    // Check boundry of outer and inner sliders
    function checkBoundry() {
        let outer = slider.getBoundingClientRect(),
            inner = innerSlider.getBoundingClientRect();

        if (parseInt(innerSlider.style.left) > 0) {
            innerSlider.style.left = "0px";
        } else if (inner.right < outer.right) {
            innerSlider.style.left = `-${inner.width - outer.width + 1000}px`;
        }
    }
};

// Invoke code
draggableSlider();


function goTo(url) {
    window.open(
        url,
        '_blank' // <- This is what makes it open in a new window.
    );
}

$(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});
AOS.init();
